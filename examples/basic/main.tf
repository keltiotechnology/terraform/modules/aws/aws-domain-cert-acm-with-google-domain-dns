provider "aws" {}
provider "google" {}

module "aws_certificates" {
  source = "../../"

  google_domain_managed_zone = "hor1zonproject.com"
  domain_name                = "test.hor1zonproject.com"
  subject_alternative_names  = ["www.test.hor1zonproject.com"]
}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 2.0"
    }
    google = {
      source  = "hashicorp/google"
      version = "4.0.0"
    }
  }
}
